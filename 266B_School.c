#include <stdio.h>
#include <string.h>

int main() {
  int n,t,i,j;
  scanf("%d %d\n",&n,&t);
  char in[n+2];
  fgets(in,n+2,stdin);

  for(i=0;i<t;i++) {
    for(j=0;j<n-1;j++) {
      if(in[j]=='B' && in[j+1]=='G') {
        in[j]='G';
        in[j+1]='B';
        j++;
      }
    }
  }

  printf("%s",in);

  return 0;
}
