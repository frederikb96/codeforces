#include <stdio.h>
#include <math.h>

int main() {
  int t,i,n;
  scanf("%d",&t);

  int y[t];

  for(i=0;i<t;i++) {
    scanf(" %d",&n);
    y[i]=((1<<n)+(1<<(n/2))-2)-((1<<n)-(1<<(n/2)));
  }
  for(i=0;i<t;i++) {
    printf("%d\n",y[i]);
  }

  return 0;
}
