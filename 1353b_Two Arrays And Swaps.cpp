#include <iostream>
#include <algorithm>

using namespace std;

int main() {
  int t;
  cin>>t;
  while(t--){
    int n,k,f;
    int *i, *j;
    cin>>n>>k;
    int a[n], b[n];
    for(f=0;f<n;f++) {
      cin>>a[f];

    }
    for(f=0;f<n;f++ ) {
      cin>>b[f];
    }
    while(k--) {
      i=min_element(a,a+n);
      j=max_element(b,b+n);
      if(*i<*j) {
        f=*i;
        *i=*j;
        *j=f;
      }
      else {
        break;
      }
    }

    int sum=0;
    for(f=0;f<n;f++) {
    sum=sum+a[f];

    }


    cout<<sum<<endl;


  }



  return 0;
}
