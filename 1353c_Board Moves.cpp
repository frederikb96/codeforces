#include <iostream>

using namespace std;

long long int calc(long long n);

int main() {
  int t;
  cin >> t;

  while(t--){

    long long n;
    cin >> n;
    cout << calc(n) << endl;


  }


  return 0;
}


long long int calc( long long n) {
  if(n<=1) {
    return 0;
  } else
  {

    return calc(n-2)+(4*n-4)*(n-1)/2*1ll;
  }

}
